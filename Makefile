ENVIRONMENT ?= "local"

test-e2e-headless:
	npm run e2e

test-e2e-interactive:
	npm run e2e:headed
