import './style.css';


/* GLOBAL VARIABLES */
let playable = true;
let hiddenPhrase;
let description;
const correctLetters = [];
const wrongLetters = [];


getRandomWikipediaTopic().then(({ title, extract }) => {
    hiddenPhrase = title.toLowerCase();
    description = extract;
    updateUI(hiddenPhrase, correctLetters, wrongLetters);
});


window.addEventListener('keydown', e => {
    if (playable) {
        let guess = e.key;
        if (/[a-z]+/.test(guess) && guess.length === 1) {
            const letter = e.key.toLowerCase();

            if (hiddenPhrase.includes(letter)) {
                if (!correctLetters.includes(letter)) {
                    correctLetters.push(letter);

                    updateUI(hiddenPhrase, correctLetters, wrongLetters);
                    checkWinOrLose(hiddenPhrase, wrongLetters);
                } else {
                    showNotification();
                }
            } else {
                if (!wrongLetters.includes(letter)) {
                    wrongLetters.push(letter);

                    updateUI(hiddenPhrase, correctLetters, wrongLetters)
                    checkWinOrLose(hiddenPhrase, wrongLetters);
                } else {
                    showNotification();
                }
            }
        }
    }
});

// Restart game and play again
document.getElementById('play-button').addEventListener('click', () => {
    playable = true;

    //  Empty arrays
    correctLetters.splice(0);
    wrongLetters.splice(0);

    getRandomWikipediaTopic().then(({ title, extract }) => {
        hiddenPhrase = title.toLowerCase();
        description = extract;
        updateUI(hiddenPhrase, correctLetters, wrongLetters);
    });

    const popup = document.getElementById('popup-container');
    popup.style.display = 'none';
});


function getRandomWikipediaTopic() {
    return new Promise((resolve, reject) => {
        let result = fetch('https://en.wikipedia.org/api/rest_v1/page/random/summary')
            .then(response => response.json())
            // TODO: remove this toLowerCase when caps/lowercase is supported in game
            .then(json => {
                return json;
            });
        resolve(result);
    });
}


function updateUI(hiddenPhrase, correctGuesses, incorrectGuesses) {
    // render wrong letter gueses
    const wrongLettersEl = document.getElementById('wrong-letters');
    wrongLettersEl.innerHTML = `
    ${wrongLetters.length > 0 ? '<p>Wrong</p>' : ''}
    ${wrongLetters.map(letter => `<span>${letter}</span>`)}`;

    // render correct letter guesses
    const phrase = document.getElementById('phrase');
    phrase.innerHTML = '';
    hiddenPhrase.split('').forEach(letter => {
        const span = document.createElement('span');
        const characterClass = /[^a-z]/.test(letter) ? 'space' : 'letter'
        span.classList.add(characterClass);

        let character = correctGuesses.includes(letter) || /[^a-z]/.test(letter) ? letter : '';
        span.innerText = character;
    
        phrase.append(span);
    });

    // update stick figure parts
    const figureParts = document.querySelectorAll('.figure-part');
    figureParts.forEach((part, index) => {
        const errors = incorrectGuesses.length;

        if (index < errors) {
            part.classList.add("visible");
            part.classList.remove("hidden");
        } else {
            part.classList.remove("visible");
            part.classList.add('hidden');
        }
    });
}


function checkWinOrLose(hiddenPhrase, incorrectGuesses) {
    // TODO: stop holding state in DOM here?
    const phrase = document.getElementById('phrase').innerText.replace(/[ \n]/g, '');

    const finalMessage = document.getElementById('final-message');
    const finalMessageRevealPhrase = document.getElementById('final-message-reveal-phrase');
    const popup = document.getElementById('popup-container');
    const phraseDescription = document.getElementById('phrase-description');

    // check if player lost
    if (incorrectGuesses.length === 6) {
        finalMessage.innerText = 'Unfortunately you lost.';
        finalMessageRevealPhrase.innerText = `...the phrase was: ${hiddenPhrase}`;
        popup.style.display = 'flex';
        playable = false;
        // TODO: display the description upon winning or losing
        phraseDescription.innerText = description;
    }
    // check if player won
    if (phrase.replace(/[^a-z]/g, '') === hiddenPhrase.replace(/[^a-z]/g, '')) {
        finalMessage.innerText = 'Congratulations! You won!';
        finalMessageRevealPhrase.innerText = '';
        popup.style.display = 'flex';
        playable = false;
        phraseDescription.innerText = description;
    }
}


function showNotification() {
    const notification = document.getElementById('notification-container');
    notification.classList.add('show');
    notification.innerText = 'You have already entered this letter'
    setTimeout(() => {
        notification.classList.remove('show');
        notification.innerText = '';
    }, 2000);
}


