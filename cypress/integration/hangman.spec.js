import { title as wikipediaTopic, extract as description } from '../fixtures/wikipediaTopic.json';


const ENVIRONMENT = Cypress.env('environment');
Cypress.config().baseUrl = ENVIRONMENT == 'prod' ? Cypress.env('prodUrl') : Cypress.env('localUrl');
const TEST_PHRASE = wikipediaTopic.toLowerCase();


const getCharIndexesFromText = (character, text) => {
    let result = Array.from(text)
        .map((char, idx) => char == character ? idx : null)
        .filter(val => val);
    return result;
}

const winGameForPlayer = (testPhrase) => {
    Array.from(testPhrase.replace(/[^a-z]/g, '')).forEach(letter => {
        cy.get('body').type(letter);
    });
}

const loseGameForPlayer = (testPhrase) => {
    const wrongGuesses = 'abcdefghijklmnopqrstuvwxyz'
        .split('')
        .filter(letter => !testPhrase.includes(letter));
    for (let i = 0; i < 6; i++) {
        cy.get('body').type(wrongGuesses[i]);
    }
}


describe("Hangman", () => {
    beforeEach(() => {
        cy.intercept('GET', 'https://en.wikipedia.org/api/rest_v1/page/random/summary',
            { fixture: 'wikipediaTopic.json' }).as('wikipediaTopic');
        cy.visit('/');
        cy.wait('@wikipediaTopic');
    });

    context('initializes the game', () => {
        it('with a dash rendered for each guessable character in the phrase', () => {
            cy.get('[data-cy=phrase]').children().should('have.length', TEST_PHRASE.length)
        });

        it('without any parts of the stick figure revealed', () => {
            cy.get('[data-cy=figure]').get('.hidden').should(hiddenParts => {
                expect(hiddenParts.length).to.eq(6);
            });
        });

        it('without any incorrect guesses in the wrong guesses box', () => {
            cy.get('[data-cy=incorrect-guesses').children().should('have.length', 0);
        });

        it('with non-alpha characters already rendered to the screen', () => {
            let nonAlphaCharacters = TEST_PHRASE.replace(/[a-z\n ]/g, '')
            cy.get('[data-cy=phrase]').should((phrase) => {
                expect(phrase.get(0).innerText.replace(/[a-z\n ]/g, '')).to.eq(nonAlphaCharacters);
            })
        });
    });

    context('when the player guesses a letter CORRECTLY', () => {
        it('the guessed letter is revealed in the dashes', () => {
            const letterGuess = 'd';
            cy.get('body').type(letterGuess);
            cy.get('[data-cy=phrase]').should((phrase) => {
                expect(phrase.get(0).innerText.replace(/[^a-z]/g, '')).to.eq(letterGuess);
            })
        });

        it('all instances of the guessed letter are revealed if it is in multiple places', () => {
            const letterGuess = 'e';
            cy.get('body').type(letterGuess);
            cy.get('[data-cy=phrase]').should(guesses => {
                const indexes = getCharIndexesFromText(letterGuess, TEST_PHRASE);
                indexes.forEach(index => {
                    expect(guesses.children().get(index).innerText).to.eq(letterGuess);
                });
            })
        });

        it('the game will not reveal a body part on the figure', () => {
            const letterGuess = 'e';
            cy.get('body').type(letterGuess);
            cy.get('[data-cy=figure]').get('.hidden').should(hiddenParts => {
                expect(hiddenParts.length).to.eq(6);
            });
        });
    });

    context('when the player guesses IN-CORRECTLY', () => {
        it('the incorrect letter guess is logged on the screen', () => {
            const letterGuess = 'x';
            cy.get('body').type(letterGuess);
            cy.get('[data-cy=incorrect-guesses').contains(letterGuess);
        });

        it('multiple times, then the game will log the multiple incorrect letter guesses', () => {
            cy.get('body').type('x').type('v');
            cy.get('[data-cy=incorrect-guesses').contains('x');
            cy.get('[data-cy=incorrect-guesses').contains('v');
        });

        it('the game will render a part of the stick figure for each incorrect guess', () => {
            cy.get('body').type('x').type('v');
            cy.get('[data-cy=figure]').get('.visible').should(visibleParts => {
                expect(visibleParts.length).to.eq(2);
            });
        });
    });

    context('gameplay will', () => {
        it('end the game if they guessed incorrectly six times, and inform the player they lost', () => {
            loseGameForPlayer(TEST_PHRASE)
            cy.get('[data-cy=final-message]').contains('Unfortunately you lost');
            cy.get('[data-cy=phrase-reveal]').contains(TEST_PHRASE);

            it('will update the UI to original state if the player chooses to play again', () => {
                cy.get('[data-cy=play-again]').click();
                cy.get('[data-cy=incorrect-guesses').children().should('have.length', 1);
            });
        });

        it('let a player play again if they choose, and re-render the UI in a blank state', () => {
            loseGameForPlayer(TEST_PHRASE);
            cy.get('[data-cy=final-message]').contains('Unfortunately you lost');
            cy.get('[data-cy=phrase-reveal]').contains(TEST_PHRASE);
            cy.get('[data-cy=play-again]').click();

            // assert
            cy.get('[data-cy=phrase]').children().should('have.length', TEST_PHRASE.length);
            cy.get('[data-cy=figure]').get('.hidden').should(hiddenParts => {
                expect(hiddenParts.length).to.eq(6);
            });
            cy.get('[data-cy=incorrect-guesses').children().should('have.length', 0);
        });

        it('lets a player guess incorrectly up to five times', () => {
            cy.get('body').type('x').type('v').type('q').type('z').type('w');
            cy.get('[data-cy=final-message]').should('have.text', '');
        });

        it('end the game and tells the player they won if they guessed the phrase before using all their chances', () => {
            winGameForPlayer(TEST_PHRASE);
            cy.get('[data-cy=final-message]').contains('Congratulations! You won!');
        });

        it('also display a description of the phrase in the modal if the player WINS', () => {
            winGameForPlayer(TEST_PHRASE);
            cy.get('[data-cy=phrase-description]').contains(description);
        });

        it('also display a description of the phrase in the modal if the player LOSES', () => {
            loseGameForPlayer(TEST_PHRASE);
            cy.get('[data-cy=phrase-description]').contains(description);
        });
    })

    context('in game notifications', () => {
        it('tells the player when they have previously guessed a letter', () => {
            cy.get('body').type('x').type('x');
            cy.get('[data-cy=notification]').contains('You have already entered this letter');
            cy.get('[data-cy=phrase]').children().should('have.length', TEST_PHRASE.length);
        });
    });
});