# Hangman Game
Select a letter to figure out a hidden phrase in a set amount of chances

## Project Specifications
* Display hangman pole and figure using SVG
* Retrieve a random phrase (article title) from Wikipedia
* Display phrase in UI with correct letters
* Display wrong letters
* Show notification when select a letter twice
* Show popup on win or lose
* Play again button to reset game

## TODOs and Goals
 * Don't download Cypress during Heroku deployment process
 * implement a spinner during the API call to Wikipedia
 * ~~display the description upon winning or losing~~
 * ~~add tests around~~:
    * ~~full phrases~~
    * ~~special characters~~
 * ~~use wiki random article API to get phrases~~
 * ~~add functions to handle:~~
    * ~~full phrases with spaces~~
    * ~~render special characters (w/o player guesssing)~~
 * ~~mock possible phrases JSON in a sensible way for testing~~
 * ~~Import possible phrases for guessing from a JSON file~~

## Cypress testing

Cypress is a framework agnostic UI testing tool. It is utterly awesome. Literally the bomb dot com.

You can call the Cypress tests to execute in the following manners:

 1. Headlessly ~*or*~ using the test runner UI.
    ```bash
    make test-e2e-headless; # runs test in the CLI
    make test-e2e-interactive # runs with the Cypress test runner UI
    ```

 2. Against your local environment ~*or*~ against the production instance.
    ```bash
    make test-e2e-headless; # tests against locally served instance
    ENVIRONMENT=prod make test-e2e-headless; # tests against production instance
    ```

    If you'd like to use the step-through functionality of the interactive test runner,
    you'll have to open the Cypress app using `npx cypress open` and click on the corresponding
    tests you'd like step through.


## Deployment

To deploy your current branch to Heroku, run the following command:
```
git push heroku master --force
```
